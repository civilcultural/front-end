# Civil Cultural

Esse é o frontend do Cultural Civil Project. ele é desenvolvido em React.js e é pensado para consumir uma API REST do [CCP-BACKEND](https://github.com/j4g3/CCP-BACKEND).

## Instruções

Todos os processos necessários para utilização da aplicação estão listados abaixo.

### Clonar

Para clonar este repositório, execute o comando abaixo, substituindo o `<usuário>` pelo seu usuário do BitBucket:

```bash
git clone https://<usuário>@bitbucket.org/civilcultural/front-end.git
```

### Instalar

Ápos fazer o clone desse repositorio rode

```bash
yarn install
```

Caso você tenha apenas o npm, execute:

```bash
npm i
```

### Configurar

Para configurar a aplicação, basta criar um arquivo chamado `.env` na pasta raiz do projeto. Este arquivo deve conter as mesmas variáveis do arquivo `.env.example`, mudando os valores de cada chave conforme a configuração desejada.

### Executar

```bash
yarn start
```

Ou

```bash
npm start
```

### Testar

Para rodar a suite de testes, execute o script `test`:

```bash
yarn test
```

Ou

```bash
npm test
```

## Feito em

* [React.js](https://pt-br.reactjs.org/docs/getting-started.html) - Biblioteca para criar interfaces web em Javascript
* [Jest](https://jestjs.io/docs/pt-BR/getting-started) - Runner para a suite de testes
