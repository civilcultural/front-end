import React, { useEffect, useState } from "react";
function ReadMore({ content }) {
  const [text, setText] = useState("");
  useEffect(() => {
    const textFor = content.substring(0, 60);
    setText(textFor);
  }, [content]);
  return <p>{text}...</p>;
}

export default ReadMore;
