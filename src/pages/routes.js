import React from "react";

import { BrowserRouter, Route } from "react-router-dom";

import Main from "./main";
import ReadPost from "./post/read";
import Dashboard from "./dashboard";
import Login from "./login";
import Redirect from "./404";

const Routes = () => (
  <BrowserRouter>
    <Route path="/" exact component={Main} />
    <Route path="/read/:title" exact component={ReadPost} />
    <Route
      path="/dashboard/:userid/"
      exact
      component={Dashboard}
      name="DashBoard"
    />
    <Route path="/dashboard" exact component={Redirect} name="redirect" />
    <Route path="/login" component={Login} />
  </BrowserRouter>
);
export default Routes;
