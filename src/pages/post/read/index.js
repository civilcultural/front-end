import React, { useEffect, useState } from "react";
import Helmet from "react-helmet";
import api from "../../../services/api";
import "./main.css";

function ReadPost({ match }) {
  const [post, setPost] = useState(null);

  useEffect(() => {
    async function getPost() {
      await api.get(`/post/${match.params.title}`).then(response => {
        setPost(response.data);
      });
    }
    getPost();
  }, [match.params.title]);
  return (
    <section>
      <Helmet title={`${post ? `${post.title} -` : ""} Civil Cultural`} />
      <header className="header">
        <span className="name">Civil Cultural</span>
      </header>
      {post ? (
        <section>
          <main className="mainRead">
            <h1 className="title">{post.title}</h1>
            <p className="description">{post.description}</p>
            <span className="author">Autor - {post.author}</span>
          </main>
        </section>
      ) : (
        <section className="not-found">Post não encontrado</section>
      )}
    </section>
  );
}

export default ReadPost;
