import React, { useEffect, useState } from "react";
import Helment from "react-helmet";
import ReadMore from "./readMore";
import api from "../../services/api";
import "./main.css";

function Main({ history }) {
  const [posts, setPosts] = useState([]);

  async function getPosts() {
    try {
      const response = await api.get("/posts");
      setPosts(response.data);
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(() => {
    getPosts();
  }, []);

  function openPost(title) {
    title = title.replace(/ /g, "-");
    history.push(`/read/${title}`);
  }

  return (
    <>
      <Helment title="Civil Cultural - Posts" />
      <header className="header">
        <span className="name">Civil Cultural</span>
      </header>
      <main className="main">
        <span className="title">New Posts</span>
        <div className="posts">
          {posts.reverse().map(post => (
            <article
              className="post"
              key={post._id}
              onClick={() => openPost(post.title)}
            >
              <header className="title">
                <h3>{post.title}</h3>
              </header>
              <main className="content">
                <ReadMore content={post.description} />
              </main>
              <footer className="footer">Author: {post.author}.</footer>
            </article>
          ))}
        </div>
      </main>
    </>
  );
}

export default Main;
